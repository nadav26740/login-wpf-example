﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LogicWpfExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }



        // -====== textbox animations -======
        // UsernameBox got focus animation
        private void UsernameBox_GotFocus(object sender, RoutedEventArgs e)
        {
            // duration
            Duration dura = new Duration(TimeSpan.FromSeconds(0.15));
            
            // defining animations
            ThicknessAnimation marginAnimation = new ThicknessAnimation(new Thickness(35, 8, 0, 0), dura);
            ColorAnimation TextUsername = new ColorAnimation((Color)ColorConverter.ConvertFromString("#7F8C8D"), dura);

            // start animations
            UsernameText.BeginAnimation(MarginProperty, marginAnimation);
            UsernameText.Foreground.BeginAnimation(SolidColorBrush.ColorProperty, TextUsername);
        }

        // password box got focus animation
        private void PasswordBox_GotFocus(object sender, RoutedEventArgs e)
        {
            // duration
            Duration dura = new Duration(TimeSpan.FromSeconds(0.15));

            // defining animations
            ThicknessAnimation marginAnimation = new ThicknessAnimation(new Thickness(35, 8, 0, 0), dura);
            ColorAnimation TextUsername = new ColorAnimation((Color)ColorConverter.ConvertFromString("#7F8C8D"), dura);

            // start animations
            PasswordText.BeginAnimation(MarginProperty, marginAnimation);
            PasswordText.Foreground.BeginAnimation(SolidColorBrush.ColorProperty, TextUsername);
        }

        // UsernameBox lost focus animation
        private void UsernameBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (UsernameBox.Text.Length == 0)
            {
                // duration
                Duration dura = new Duration(TimeSpan.FromSeconds(0.15));

                // defining animations
                ThicknessAnimation marginAnimation = new ThicknessAnimation(new Thickness(30, 30, 0, 0), dura);
                ColorAnimation TextUsername = new ColorAnimation((Color)ColorConverter.ConvertFromString("#ECEFF1"), dura);
                
                // start animations
                UsernameText.BeginAnimation(MarginProperty, marginAnimation);
                UsernameText.Foreground.BeginAnimation(SolidColorBrush.ColorProperty, TextUsername);
            }
        }

        // PasswordBox lost focus animation
        private void PasswordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            // duration
            if (PasswordBox.Password.Length == 0)
            {
                Duration dura = new Duration(TimeSpan.FromSeconds(0.15));

                // defining animations
                ThicknessAnimation marginAnimation = new ThicknessAnimation(new Thickness(30, 30, 0, 0), dura);
                ColorAnimation TextUsername = new ColorAnimation((Color)ColorConverter.ConvertFromString("#ECEFF1"), dura);

                // start animations
                PasswordText.BeginAnimation(MarginProperty, marginAnimation);
                PasswordText.Foreground.BeginAnimation(SolidColorBrush.ColorProperty, TextUsername);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (UsernameBox.Text.Length == 0 || PasswordBox.Password.Length == 0)
            {
                MessageBox.Show("Make sure you fill all the box");
                return;
            }
            ImageBrush b = new ImageBrush();
            b.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/Pictures/avatardefault_92824.png"));
            UserIcon.Background = b;

            HelloUserContant.Content = "Hello, " + UsernameBox.Text;
        }

        // -================================


        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.DragMove();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }
    }
}
