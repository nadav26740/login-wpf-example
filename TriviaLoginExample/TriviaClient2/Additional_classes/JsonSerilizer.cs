﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient2
{
    // Classes of media Sending
    public class LoginRequestClass
    {
        public string password { get; set; }
        public string username { get; set; }
    }

    public class StatusClass
    {
        public int status { get; set; }

        public StatusClass()
        {
            status = 0;
        }
    }

    public class RoomReqClass
    {
        public uint roomId { get; set; }
    }

    public class CreateRoomRequest
    {
        public string roomName { get; set; }
        public uint maxUsers { get; set; }
        public uint questionCount { get; set; }
        public uint answerTimeout { get; set; }
    };

    public class SignUpClass
    {
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
    }

    public class ErrorClass
    {
        public string Message { get; set; }
    }

    public class AnswerClassSubmit
    {
        public string answer { get; set; }
    }
    // serilizer

    public class EchoRequestClass
    { 
        public string msg { get; set; }
    }


    public class JsonSerilizer
    {
        public static msgrcv SerielizLeaveRoom()
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.LEAVE_ROOM_REQ; // Adding code

            // Login request builder
            StatusClass temp = new StatusClass();

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerielizCreateRoom(string roomName, uint maxUsers, uint questionCount, uint answerTimeout)
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.CREATEROOM_REQ; // Adding code

            // Login request builder
            CreateRoomRequest temp = new CreateRoomRequest();
            temp.roomName = roomName;
            temp.maxUsers = maxUsers;
            temp.questionCount = questionCount;
            temp.answerTimeout = answerTimeout;

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerielzLogin(string password, string username)
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.LOGIN_REQ; // Adding code

            // Login request builder
            LoginRequestClass temp = new LoginRequestClass();
            temp.password = password;
            temp.username = username;
            
            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerilizeGetRoom() // TODO
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.GETROOMS_REQ; // Adding code

            // Login request builder
            StatusClass temp = new StatusClass();

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerilizeLogout() // TODO
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.LOGOUT_REQ; // Adding code

            // Login request builder
            StatusClass temp = new StatusClass();

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerielizRoomPlayers(uint RoomID) // TODO
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.ROOM_PLAYERS_REQ; // Adding code

            // RoomId request builder
            RoomReqClass temp = new RoomReqClass();
            temp.roomId = RoomID;

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerielizJoinRoom(uint RoomID) // TODO 
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.JOINROOM_REQ; // Adding code

            // RoomId request builder
            RoomReqClass temp = new RoomReqClass();
            temp.roomId = RoomID;

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerielizSignUp(string username, string password, string email)
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.SIGNUP_REQ; // Adding code

            // RoomId request builder
            SignUpClass temp = new SignUpClass();
            temp.username = username;
            temp.password = password;
            temp.email = email;

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerielizPersonalStats()
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.PERSONALSTAT_REQ; // Adding code

            // RoomId request builder
            StatusClass temp = new StatusClass();

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerielizHighScore()
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.HIGHSCORE_REQ; // Adding code

            // RoomId request builder
            StatusClass temp = new StatusClass();

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerielizStartRoom(uint roomId)
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.START_ROOM_REQ; // Adding code

            // RoomId request builder
            RoomReqClass temp = new RoomReqClass();
            temp.roomId = roomId;

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerielizPlayerUpdateReq()
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.PLAYER_TICK_REQ; // Adding code

            // RoomId request builder
            StatusClass temp = new StatusClass();

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg   
        }

        public static msgrcv SerielizSubmitAnswer(string answer)
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.SUBMIT_ANSWER_REQ; // Adding code

            // RoomId request builder
            AnswerClassSubmit temp = new AnswerClassSubmit();
            temp.answer = answer;

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerielizGetScoreBoard()
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.GET_SCOREBOARD_REQ; // Adding code

            // RoomId request builder
            StatusClass temp = new StatusClass();

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }

        public static msgrcv SerielizRoomState(uint id)
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.GET_ROOM_STATE_REQ; // Adding code

            // RoomId request builder
            RoomReqClass temp = new RoomReqClass();
            temp.roomId = id;

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }
        
        public static msgrcv SerielizEcho(string msg)
        {
            msgrcv ret = new msgrcv();
            ret.code = (byte)CODES.ECHO_REQ; // Adding code

            // RoomId request builder
            EchoRequestClass temp = new EchoRequestClass();
            temp.msg = msg;

            // convert to json
            ret.msg = JsonConvert.SerializeObject(temp);
            return ret; // returning the full msg
        }
    }
}
