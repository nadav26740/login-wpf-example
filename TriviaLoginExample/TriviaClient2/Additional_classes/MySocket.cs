﻿using System;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace TriviaClient2
{
    enum CODES:byte
    {
        // ==========================================
        // Login / signup / logout
        ACC_LOGIN = 1,      // Server -> Client	// 1
        LOGIN_REQ,          // Client -> Server	// 2

        ERROR_SER,          // Server -> Client	// 3
        CLIENT_ERROR,       // Client -> Server	// 4

        ACC_SIGNUP,         // Server -> Client	// 5
        SIGNUP_REQ,         // Client -> Server	// 6

        ACC_LOGOUT,         // Server -> Client	// 7
        LOGOUT_REQ,         // Client -> Server	// 8
                            // ==========================================

        // ==========================================
        // ROOMS
        ACC_GERROOMS,       // Server -> Client	// 9
        GETROOMS_REQ,       // Client -> Server	// 10

        ACC_ROOM_PLAYERS,   // Server -> Client // 11
        ROOM_PLAYERS_REQ,   // Client -> Server // 12

        ACC_JOINROOM,       // Server -> Client	// 13
        JOINROOM_REQ,       // Client -> Server	// 14

        ACC_CREATEROOM,     // Server -> Client	// 15
        CREATEROOM_REQ,     // Client -> Server	// 16
                            // ==========================================

        // ==========================================
        // STATISTICS
        ACC_HIGHSCORE,      // Server -> Client	// 17
        HIGHSCORE_REQ,      // Client -> Server	// 18

        ACC_PERSONALSTAT,   // Server -> Client // 19
        PERSONALSTAT_REQ,   // Client -> Server // 20
                            // ==========================================

        // ==========================================
        // Finishing
        LEAVE_ROOM_REQ,     // Client -> Server	// 21
        ACC_LEAVE_ROOM,     // Server -> Client	// 22

        GET_ROOM_STATE_REQ, // client -> Server // 23
        ACC_GET_ROOM_STATE, // Server -> Client // 24

        START_ROOM_REQ,     // Client -> Server // 25
        ACC_START_ROOM,     // Server -> Client // 26
        ROOM_ERROR,         // Server -> Client // 27 // will force the client to leave the server

        // gameplay 
        PLAYER_TICK_REQ,    // Client -> Server // 28	
        ACC_PLAYER_TICK,    // Server -> Client // 29

        GET_SCOREBOARD_REQ, // Client -> Server // 30
        ACC_GET_SCOREBOARD, // Server -> Client // 31

        SUBMIT_ANSWER_REQ,  // Client -> Client // 32
        ACC_SUBMIT_ANSWER,  // Server -> Client // 33

        ECHO_REQ, // 
        ACC_ECHO
        // ==========================================
    };

    public struct msgrcv
    {

        public msgrcv(byte cod, string str)
        {
            this.msg = str;
            this.code = cod;
        }

        public string msg { get; set; }
        public byte code { get; set; }
    }


    public class MySocket
    {   
        
        private TcpClient TCPclient;
        private NetworkStream TCPstream;

        public MySocket(string server, int port)
        {
            try
            {
                // Create a TcpClient.
                // Note, for this client to work you need to have a TcpServer 
                // connected to the same address as specified by the server, port
                // combination.
                if(port > 65535 || port < 0)
                {
                    throw new ArgumentNullException ("Iliegal Port");
                }



                TCPclient = new TcpClient(server, port);
                TCPstream = TCPclient.GetStream();



            }
            catch (ArgumentNullException e)
            {
                throw new ArgumentNullException($"ArgumentNullException: {e}");
            }
        }

        ~MySocket()
        {
            try
            {
                // Close everything.
                TCPstream?.Close();
            }
            catch {     }

            try
            {
                // Close everything.
                TCPclient?.Close();
            }
            catch { }

        }


        public msgrcv SendMessageReceive(msgrcv ToSend)
        {
            // building the msg!
            byte[] bytesSent = new byte[ToSend.msg.Length + 1 + 4];
            bytesSent[0] = ToSend.code; // inserting the code into the 

            msgrcv recive = new msgrcv(0, "");

            // inserting the length into the msg
            for (int i = 0; i < 4; i++)
            {
                bytesSent[i + 1] = BitConverter.GetBytes(ToSend.msg.Length)[i];
            }

            // inserting the data into the msg
            for (int i = 0; i < ToSend.msg.Length; i++)
            {
                bytesSent[i + 1 + 4] = Encoding.ASCII.GetBytes(ToSend.msg)[i];
            }



            // sending the msg
            // Translate the passed message into ASCII and store it as a Byte array. Any encoding can be used as long as it's consistent with the server.

            // Get a client stream for reading and writing.
            //  Stream stream = client.GetStream();

            // Send the message to the connected TcpServer. 
            TCPstream.Write(bytesSent, 0, bytesSent.Length);

            // Receive the TcpServer.response. This is all optional and can be removed if you aren't recieving a response.
            // Buffer to store the response bytes.
            byte[] data = new byte[5];
            TCPstream.Read(data, 0, 1); 
            recive.code = data[0];

            if(TCPstream.Read(data, 0, 4) != 4)
            {
                MessageBox.Show("Error in reading the Message size");
            }

            data = new byte[BitConverter.ToInt32(data, 0)];

            // String to store the response ASCII representation.

            // Read the first batch of the TcpServer response bytes.
            int bytes = TCPstream.Read(data, 0, data.Length);
            recive.msg = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
            return recive;
        }




        //                      === Example method === 
        // This method requests the home page content for the specified server.
        //private static string SocketSendReceive(string server, int port)
        //{
            //                  === Example ===
            // string request = "GET / HTTP/1.1\r\nHost: " + server +
            //   "\r\nConnection: Close\r\n\r\n";
            // Byte[] bytesSent = Encoding.ASCII.GetBytes(request);
            // Byte[] bytesReceived = new Byte[256];
            // string page = "";
            // Socket s = ConnectSocket(server, port)

            // Create a socket connection with the specified server and port.


            // if (s == null)
            //     return ("Connection failed");

            // Send request to the server.
            //s.Send(bytesSent, bytesSent.Length, 0);

            // Receive the server home page content.
            // int bytes = 0;

            // The following will block until the page is transmitted.
        //    do
        //    {
        //        bytes = s.Receive(bytesReceived, bytesReceived.Length, 0);
        //        page = page + Encoding.ASCII.GetString(bytesReceived, 0, bytes);
        //    }
        //    while (bytes > 0);
            

        //    return page;
        // } 
    }
}
