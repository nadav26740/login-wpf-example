﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TriviaClient2.Additional_classes
{
    public delegate void UiStyleColorChangede(StyleColor Style, bool Dark_mode);

    public enum StyleColor
    {
        Blue, Pink
    }

    public struct user_info
    {
        string username { get; set; }
        string currRoomName { get; set; }
    }

    public enum FormsId : byte
    {
        MAIN_MENU = 1,
        ROOMS_FORM,
        IN_GAME
    }

    public static class UiEngine
    {
        // socket inforamtion
        private const string def_ip = "127.0.0.1";
        private const int def_port = 9931;

        // pointer for function event of color style change function args Style_color and is dark_mode
        public static event UiStyleColorChangede On_Color_Changed; 

        // MainWindow and ui shitty vars
        public static MainWindow main_win { set; get; }
        public static string Player_username { get; set; }
        public static MySocket server_Connection_socket;

        public static void ChangeColorStyle(StyleColor Style, bool Dark_mode)
        {
            On_Color_Changed?.Invoke(Style, Dark_mode);
        }

        public static void ChangeForm(FormsId forid)
        {

        }

        public static msgrcv SendMsg(msgrcv currmsg)
        {
            if (server_Connection_socket == null)
            {
                connectSocket();
            }

            if (server_Connection_socket != null)
            {
                Debug.Print("Sending: " + currmsg.code + " \t: " + currmsg.msg);
                currmsg = server_Connection_socket.SendMessageReceive(currmsg);
                Debug.Print("Recive:  " + currmsg.code + " \t: " + currmsg.msg);
                return currmsg;

            }
            MessageBox.Show("Socket Error", "Can't open Socket");
            Debug.Print("Error Socket Failure");
            return new msgrcv();

        }

        public static void connectSocket()
        {
            connectSocket(def_ip, def_port);
        }


        public static void connectSocket(string ServerIp, int ServerPort)
        {
            Debug.Print("Trying To connect to " + ServerIp + ":" + ServerPort);
            try
            {
                server_Connection_socket = new MySocket(ServerIp, ServerPort);
                main_win.ChangeConnectionStatus(true);
                //Debug.Print("Connected to " + ServerIp + " : " + ServerPort);
                Debug.Print("Successed To connect to " + ServerIp + ":" + ServerPort);
            }
            catch
            {
                server_Connection_socket = null;
                //Debug.Print("Failed to connect to " + ServerIp + " : " + ServerPort);
                main_win.ChangeConnectionStatus(false);
                Debug.Print("Failed To connect to " + ServerIp + ":" + ServerPort);
                //Program.OpenMessage("Socket Error", "Can't open Socket");
            }
        }
    }
}
