﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

public class StatusClass
{
    public uint status { get; set; }
}

public class ErrorResponse
{
    public string message { get; set; }
}

public partial class QuestionTemperatures
{
    [JsonProperty("response_code")]
    public long ResponseCode { get; set; }

    [JsonProperty("results")]
    public QuestionResult[] Results { get; set; }
}

public partial class QuestionResult
{
    [JsonProperty("category")]
    public string Category { get; set; }

    [JsonProperty("type")]
    public string Type { get; set; }

    [JsonProperty("difficulty")]
    public string Difficulty { get; set; }

    [JsonProperty("question")]
    public string Question { get; set; }

    [JsonProperty("correct_answer")]
    public string CorrectAnswer { get; set; }

    [JsonProperty("incorrect_answers")]
    public string[] IncorrectAnswers { get; set; }
}




public class RoomData
{
    public uint id; // on creation leave empty the id choosen by the RoomManager
    public string name { get; set; }
    public uint maxPlayers { get; set; }
    public uint numOfQuestionsInGame { get; set; }
    public uint timePerQuestion { get; set; }
    public uint isActive { get; set; }
    public string admin { get; set; }

    public override bool Equals(Object obj)
   {
      //Check for null and compare run-time types.
      if ((obj == null) || ! this.GetType().Equals(obj.GetType()))
      {
         return false;
      }
        
      else {
            RoomData r = (RoomData) obj;
            return (r.id == this.id) &&
                (r.name == this.name) &&
                (r.numOfQuestionsInGame == this.numOfQuestionsInGame) &&
                (r.timePerQuestion == this.timePerQuestion) &&
                (r.isActive == this.isActive) &&
                (r.admin == this.admin);
      }
   }
}

public class GetRoomResponse
{
    public uint status { get; set; }
    public uint size { get; set; }
    public RoomData[] room { get; set; }
}

public class GetPlayersInRoomResponse
{
    public uint size { get; set; }
    // public std::vector<std::string> players;
    public string[] player { get; set; }
}

public class RoomStateResponse
{
    public uint status { get; set; }
    public string admin { get; set; }
    public uint isActive { get; set; }
    public uint maxPlayers { get; set; }
    public uint numOfQuestionsInGame { get; set; }
    public string[] players { get; set; }
    public uint timePerQuestion { get; set; }
}

public class GetStatisticsResponse
{
    public uint status { get; set; }
    //std::vector<std::string> statistics;
    public uint size { get; set; }
    public string[] statistics { get; set; }
}

public class Player_Results
{
    public string Username { get; set; }
    public uint CurrectAnswers { get; set; }
    public uint WrongAnswers { get; set; }
    public float AvgTimePerAnswer { get; set; }
    public uint Points { get; set; }
    public bool lastQuestionRight { get; set; }
}

public class PlayerUpdateResponse
{
    
    [JsonProperty("currForm")]
    public int curr_form { get; set; }      // what form to show
    
    public int maxtime { get; set; }        // max time per form
    public int timepast { get; set; }       // how much time past
    public string CurrQuestJson { get; set; }
    public int currQuestionNum { get; set; }
    public int maxQuestions { get; set; }   // max questions in room
    public int currpoints { get; set; }		// currect player points
}


public class GetCreateRoomResponse
{
    public uint id { get; set; }
}

public class ScoreBoardResponse
{
    public Player_Results[] playerResults { get; set; }
}

namespace TriviaClient2
{

    class jsonDeserilizer
    {
        //1
        public static StatusClass DeserielizLogin(string msg)
        {
            StatusClass siR = JsonConvert.DeserializeObject<StatusClass>(msg);
           return siR;
        }

        //2
        public static ErrorResponse DeserielizError(string msg)
        {
            ErrorResponse siR = JsonConvert.DeserializeObject<ErrorResponse>(msg);
            return siR;
        }

        //3
        public static StatusClass DeserielizSignup(string msg)
        {
            StatusClass siR = JsonConvert.DeserializeObject<StatusClass>(msg);
            return siR;
        }

        //4
        public static StatusClass DeserielizLogOut(string msg)
        {
            StatusClass siR = JsonConvert.DeserializeObject<StatusClass>(msg);
            return siR;
        }

        //5
        public static RoomData DeserielizRoomData(string msg)
        {
            RoomData siR = JsonConvert.DeserializeObject<RoomData>(msg);
            return siR;
        }

        //6
        public static GetRoomResponse DeserielizGetRooms(string msg)
        {
            GetRoomResponse siR = JsonConvert.DeserializeObject<GetRoomResponse>(msg);
            return siR;
        }

        //7
        public static GetPlayersInRoomResponse DeserielizPlayersInRoom(string msg)
        {
            GetPlayersInRoomResponse siR = JsonConvert.DeserializeObject<GetPlayersInRoomResponse>(msg);
            return siR;
        }
        //8
        public static StatusClass DeserielizJoinRoom(string msg)
        {
            StatusClass siR = JsonConvert.DeserializeObject<StatusClass>(msg);
            return siR;
        }
        //9
        public static GetCreateRoomResponse DeserielizCreateRoom(string msg)
        {
            GetCreateRoomResponse siR = JsonConvert.DeserializeObject<GetCreateRoomResponse>(msg);
            return siR;
        }
        //10
        public static GetStatisticsResponse DeserielizGetStatistics(string msg)
        {
            GetStatisticsResponse siR = JsonConvert.DeserializeObject<GetStatisticsResponse>(msg);
            return siR;
        }

        public static StatusClass DeserielizStartRoom(string msg)
        {
            StatusClass siR = JsonConvert.DeserializeObject<StatusClass>(msg);
            return siR;
        }

        public static StatusClass DeserielizSubmitAnswer(string msg)
        {
            StatusClass siR = JsonConvert.DeserializeObject<StatusClass>(msg);
            return siR;
        }

        public static ScoreBoardResponse DeserielizScoreBoard(string msg)
        {
            ScoreBoardResponse siR = JsonConvert.DeserializeObject<ScoreBoardResponse>(msg);
            return siR;
        }

        public static PlayerUpdateResponse DeserielizPlayerUpdate(string msg)
        {
            PlayerUpdateResponse siR = JsonConvert.DeserializeObject<PlayerUpdateResponse>(msg);
            return siR;
        }

        public static RoomStateResponse DeserielizRoomState(string msg)
        {
            RoomStateResponse siR = JsonConvert.DeserializeObject<RoomStateResponse>(msg);
            return siR;
        }

        public static QuestionResult DeserielizQuestionJson(string msg)
        {
            QuestionTemperatures siR = JsonConvert.DeserializeObject<QuestionTemperatures>(msg);
            return siR.Results[0];
        }

        public static StatusClass DeserielizEchoResponse(string msg)
        {
            StatusClass siR = JsonConvert.DeserializeObject<StatusClass>(msg);
            return siR;
        }

    }
}
