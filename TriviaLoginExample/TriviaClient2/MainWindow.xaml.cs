﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TriviaClient2.Additional_classes;

namespace TriviaClient2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        
        // animations varibles
        const double Polygons_Speed = 0.3;

        DispatcherTimer BackTimer = new DispatcherTimer();
        List<Polygon> polygons = new List<Polygon>();


        // setting varibles
        bool animation_is_enabled = true;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += WhenUiLoaded;
            Unloaded += WhenUiUnloaded;

            BackTimer.Tick += BackgroundTick;
            BackTimer.Interval = TimeSpan.FromMilliseconds(45);
            Additional_classes.UiEngine.main_win = this;
        }


        // for the animations only currently
        public void BackgroundTick(object sender, EventArgs e)
        {
            int counter = 0;
            RotateTransform rotateTransform1;
            foreach (Polygon item in polygons)
            {
                counter++;
                // rotateTransform1 = new RotateTransform((27 * (counter > polygons.Count / 2 ? counter : -counter)) + (Canvas.GetTop(item) * Polygons_Speed));
                // making the polygons go up in diffrent speeds
                Canvas.SetTop(item, Canvas.GetTop(item) - (Polygons_Speed * counter * Polygons_Speed));
                // item.RenderTransform = rotateTransform1;
                if (Canvas.GetTop(item) <= 0)
                {
                    // Debug.Print("item got to top stuck moving to " + Background_Shapes_canvas.ActualHeight);
                    Canvas.SetTop(item, Background_Shapes_canvas.ActualHeight);
                }
            }
        }

        public async void StyleColorChanged(StyleColor Style, bool Dark_mode)
        {
            if (Style == StyleColor.Pink)
            {
                this.Resources["BackWaveGradient"] = this.Resources["BackPinkWaveGradient"];
                this.Resources["FrontWaveGradient"] = this.Resources["FrontPinkWaveGradient"];
            }
            else if (Style == StyleColor.Blue)
            {
                this.Resources["BackWaveGradient"] = this.Resources["BackBlueWaveGradient"];
                this.Resources["FrontWaveGradient"] = this.Resources["FrontBlueWaveGradient"];
            }

            // BackgroundColor
            if (Dark_mode)
            {
                // DarkModeBackground
                this.Resources["BackgroundColor"] = ((SolidColorBrush)this.Resources["DarkModeBackground"]).Clone();
                this.Resources["TextboxBackgroundColor"] = ((SolidColorBrush)this.Resources["LightModeBackground"]).Clone();
                this.Resources["TextboxForegroundColor"] = ((SolidColorBrush)this.Resources["DarkModeBackground"]).Clone();

            }
            else
            {
                // LightModeBackground
                this.Resources["BackgroundColor"] = ((SolidColorBrush)this.Resources["LightModeBackground"]).Clone();
                this.Resources["TextboxBackgroundColor"] = ((SolidColorBrush)this.Resources["DarkModeBackground"]).Clone();
                this.Resources["TextboxForegroundColor"] = ((SolidColorBrush)this.Resources["LightModeBackground"]).Clone();
            }
        }

        public async void WhenUiUnloaded(object sender, RoutedEventArgs e)
        {
            Additional_classes.UiEngine.On_Color_Changed -= StyleColorChanged;
            if (BackTimer.IsEnabled)
                BackTimer.Stop();
        }

        public async void WhenUiLoaded(object sender, RoutedEventArgs e)
        {
            Additional_classes.UiEngine.On_Color_Changed += StyleColorChanged;

            add_polygon_to_list();
            if (animation_is_enabled)
                BackTimer.Start();

            new Task(UiEngine.connectSocket).Start();
            
            Additional_classes.UiEngine.ChangeColorStyle(StyleColor.Blue, true);

        }

        public async void add_polygon_to_list()
        {
            // adding all the polygons to the list so we will be able to work on them ez
            foreach (var item in Background_Shapes_canvas.Children)
            {
                if (item is Polygon)
                {
                    polygons.Add(item as Polygon);
                    
                    if (!animation_is_enabled)
                        (item as Polygon).Effect = null;

                    Debug.WriteLine("kill me please"); // Debug 
                }
            }
        }

        private void UsernameBox_GotFocus(object sender, RoutedEventArgs e)
        {
            // duration
            Duration dura = new Duration(TimeSpan.FromSeconds(0.15));

            // defining animations
            ThicknessAnimation marginAnimation = new ThicknessAnimation(new Thickness(35, 30, 0, 0), dura);
            ColorAnimation TextUsername = new ColorAnimation(((SolidColorBrush)this.Resources["TextboxBackgroundColor"]).Color, dura);

            //// start animations
            UsernameText.BeginAnimation(MarginProperty, marginAnimation);
            UsernameText.Foreground.BeginAnimation(SolidColorBrush.ColorProperty, TextUsername);
        }

        // password box got focus animation
        private void PasswordBox_GotFocus(object sender, RoutedEventArgs e)
        {
            // duration
            Duration dura = new Duration(TimeSpan.FromSeconds(0.15));

            // defining animations
            ThicknessAnimation marginAnimation = new ThicknessAnimation(new Thickness(35, 30, 0, 0), dura);
            ColorAnimation TextUsername = new ColorAnimation(((SolidColorBrush)this.Resources["TextboxBackgroundColor"]).Color, dura);

            // start animations
            PasswordText.BeginAnimation(MarginProperty, marginAnimation);
            PasswordText.Foreground.BeginAnimation(SolidColorBrush.ColorProperty, TextUsername);
        }

        // UsernameBox lost focus animation
        private void UsernameBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (UsernameBox.Text.Length == 0)
            {
                // duration
                Duration dura = new Duration(TimeSpan.FromSeconds(0.15));

                // defining animations
                ThicknessAnimation marginAnimation = new ThicknessAnimation(new Thickness(30, 60, 0, 0), dura);
                ColorAnimation TextUsername = new ColorAnimation(((SolidColorBrush)this.Resources["TextboxForegroundColor"]).Color, dura);

                // start animations
                UsernameText.BeginAnimation(MarginProperty, marginAnimation);
                UsernameText.Foreground.BeginAnimation(SolidColorBrush.ColorProperty, TextUsername);
            }
        }

        //PasswordBox lost focus animation
        private void PasswordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            // duration
            if (PasswordBox.Password.Length == 0)
            {
                Duration dura = new Duration(TimeSpan.FromSeconds(0.15));

                // defining animations
                ThicknessAnimation marginAnimation = new ThicknessAnimation(new Thickness(30, 60, 0, 0), dura);
                ColorAnimation TextUsername = new ColorAnimation(((SolidColorBrush)this.Resources["TextboxForegroundColor"]).Color, dura);

                // start animations
                PasswordText.BeginAnimation(MarginProperty, marginAnimation);
                PasswordText.Foreground.BeginAnimation(SolidColorBrush.ColorProperty, TextUsername);
            }
        }

        // TODO
        private void PlayButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            msgrcv msg;
            
            // sending to the server the password and username in json format
            msg = Additional_classes.UiEngine.SendMsg(
                JsonSerilizer.SerielzLogin(PasswordBox.Password, UsernameBox.Text));

            // checking if the recived message from the server said if we have connected or not
            if (msg.code == (byte)CODES.ACC_LOGIN && jsonDeserilizer.DeserielizLogin(msg.msg).status == 0)
            {
                Additional_classes.UiEngine.Player_username = UsernameBox.Text;

                // changing the server connection
                Additional_classes.UiEngine.ChangeForm(FormsId.MAIN_MENU);
                MessageBox.Show("successed to connect"); // for now
            }
            else if (msg.code == (byte)CODES.ERROR_SER)
            {
                MessageBox.Show(jsonDeserilizer.DeserielizError(msg.msg).message, "Error");

            }
        }

        private void ExitButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            App.Current.Shutdown();
        }

        // changing the status apperance in the window for the connection
        public void ChangeConnectionStatus(bool Connection_status)
        {
            this.Dispatcher.Invoke(() =>
            {
                if (Connection_status)
                {
                    ServerStatus.Text = "Online";
                    ServerStatus.Foreground = (LinearGradientBrush)this.FindResource("OnlineLinearColor");
                }
                else
                {
                    ServerStatus.Text = "Offline";
                    ServerStatus.Foreground = (LinearGradientBrush)this.FindResource("OfflineLinearColor");
                }
            });
        }

        private void TopBarGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.DragMove();
            }
        }

        private void CloseOpenSignup()
        {
            this.Dispatcher.Invoke(() =>
            {
                Debug.Write("OpenClose Signup Button Pressed\n SignupForm width = " + Signup_Border.ActualWidth + "\n");
                DoubleAnimation OpenCloseAnimation;
                // checking in what status the signup is
                if (Signup_Border.ActualWidth > 80)
                {
                    LoginGrid.IsEnabled = true;
                    LoginGrid.IsHitTestVisible = true;
                    SignUpForm.IsHitTestVisible = true;
                    OpenCloseAnimation = new DoubleAnimation(50, TimeSpan.FromSeconds(1.5));
                }
                else
                {
                    // disabling the login form
                    LoginGrid.IsEnabled = false;
                    LoginGrid.IsHitTestVisible = false;

                    // enabling the signup form
                    SignUpForm.IsHitTestVisible = true;
                    SignUpForm.Opacity = 1;
                    SignUp_Message.Opacity = 0;
                    OpenCloseAnimation = new DoubleAnimation(1150, TimeSpan.FromSeconds(1.5));
                }

                Signup_Border.BeginAnimation(WidthProperty, OpenCloseAnimation);
            });
        }

        private void CloseOpenSignup(int delay)
        {

        }

        private void Open_signup_button(object sender, MouseButtonEventArgs e)
        {
            CloseOpenSignup();

        }

        private void SignupButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            msgrcv msg;

            // sending the signup message to the server
            SignUpForm.IsHitTestVisible = false;

            Debug.Print("Trying to signup");
            msg = Additional_classes.UiEngine.SendMsg(
                JsonSerilizer.SerielizSignUp(UsernameBox_Signup.Text, PasswordBox_Signup.Password, EmailBox_Signup.Text));

            if (msg.code == (byte)CODES.ACC_SIGNUP && jsonDeserilizer.DeserielizSignup(msg.msg).status == 0)
            {
                SignUp_Message.Content = ("You Signed Up Successfuly!");
            }
            else if (msg.code == (byte)CODES.ERROR_SER)
            {
                Debug.Print(jsonDeserilizer.DeserielizError(msg.msg).message + " Failed To signup");
                SignUp_Message.Content = "Error" + jsonDeserilizer.DeserielizError(msg.msg).message;
            }

            DoubleAnimation OpacityAnimationSignup = new DoubleAnimation(0, TimeSpan.FromSeconds(1.5));
            DoubleAnimation OpacityMessageAnimation = new DoubleAnimation(1, TimeSpan.FromSeconds(3));

            SignUpForm.BeginAnimation(OpacityProperty, OpacityAnimationSignup);
            SignUp_Message.BeginAnimation(OpacityProperty, OpacityMessageAnimation);
        }
    }
}
